import sqlite3
import unittest
import os

from dktotoolkit.sqlite3 import recursive_sql

from glob_var import DB_TEST, SQL_SCRIPT

# coverage run -m unittest the_file.py



# Classe de tests unitaires : Creation des bdd
class TestUnit_CreateDB(unittest.TestCase):

    def setUp(self):

        os.makedirs(os.path.dirname(DB_TEST), exist_ok=True)

        # Connexion à la base de données SQLite
        self.connexion = sqlite3.connect(DB_TEST)
        self.cursor = self.connexion.cursor()

        # Exécution du script SQL
        scripts = recursive_sql(SQL_SCRIPT)
        scripts = scripts if isinstance(scripts, list) else [scripts,]

        for elt in scripts:
            self.cursor.executescript(elt)
        # endFor
    #


    def tearDown(self):
        # Fermeture du curseur et de la connexion
        self.cursor.close()
        self.connexion.close()

        os.remove(DB_TEST)
    #


    def test_languages_table_exists(self):
        # Vérifier si la table "cec_languages" existe
        self.cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='cec_languages'")
        result = self.cursor.fetchone()
        self.assertIsNotNone(result)
    #


    def test_treeref_table_exists(self):
        # Vérifier si la table "cec_treeref" existe
        self.cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='cec_treeref'")
        result = self.cursor.fetchone()
        self.assertIsNotNone(result)

    def test_treename_table_exists(self):
        # Vérifier si la table "cec_treename" existe
        self.cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='cec_treename'")
        result = self.cursor.fetchone()
        self.assertIsNotNone(result)
    #


    def test_content_table_exists(self):
        # Vérifier si la table "cec_content" existe
        self.cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='cec_content'")
        result = self.cursor.fetchone()
        self.assertIsNotNone(result)
    #


    def test_references_table_exists(self):
        # Vérifier si la table "cec_reference" existe
        self.cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='cec_reference'")
        result = self.cursor.fetchone()
        self.assertIsNotNone(result)
    #


    def test_references_refname_exists(self):
        # Vérifier si la table "cec_refname" existe
        self.cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='cec_refname'")
        result = self.cursor.fetchone()
        self.assertIsNotNone(result)
    #


    def test_references_adddatas_exists(self):
        # Vérifier si la table "cec_add_datas" existe
        self.cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='cec_add_datas'")
        result = self.cursor.fetchone()
        self.assertIsNotNone(result)
    #

#

# Exécution des tests
if __name__ == '__main__':
    # Lancer les tests avec la couverture de code
    unittest.main()

