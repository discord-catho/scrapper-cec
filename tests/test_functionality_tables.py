import sqlite3
import unittest
# import coverage
import os

from dktotoolkit.sqlite3 import recursive_sql

from glob_var import DB_TEST, SQL_SCRIPT



# Classe de tests fonctionnels
class TestFunctionality_Tables(unittest.TestCase):

    def setUp(self):

        os.makedirs(os.path.dirname(DB_TEST), exist_ok=True)

        # Connexion à la base de données SQLite
        self.connexion = sqlite3.connect(DB_TEST)
        self.cursor = self.connexion.cursor()

        # Exécution du script SQL
        scripts = recursive_sql(SQL_SCRIPT)
        scripts = scripts if isinstance(scripts, list) else [scripts,]

        for elt in scripts:
            self.cursor.executescript(elt)
        # endFor
    #


    def tearDown(self):
        # Fermeture du curseur et de la connexion
        self.cursor.close()
        self.connexion.close()
        os.remove(DB_TEST)

    def test_insert_language(self):
        # Insérer une langue dans la table "cec_languages"
        self.cursor.execute("INSERT INTO cec_languages (language) VALUES ('French')")
        self.connexion.commit()

        # Vérifier si la langue a été insérée avec succès
        self.cursor.execute("SELECT * FROM cec_languages WHERE language='French'")
        result = self.cursor.fetchone()
        self.assertIsNotNone(result)


    def test_cec_add_datas_trigger(self):
        # Insérer des données dans la table "cec_add_datas"
        self.cursor.execute("INSERT INTO cec_add_datas (id_cec, language, title_name, section_name, id_treeref, content, reference_full, reference_acc) VALUES (2, 'French', 'Title', 'Section', 2, 'Content', 'Reference Full', 'Reference Acc')")
        self.connexion.commit()

        # Vérifier si les données ont été dispersées dans les autres tables
        # Vérifier si la langue a été insérée dans la table "cec_languages"
        self.cursor.execute("SELECT * FROM cec_languages WHERE language='French'")
        result = self.cursor.fetchall()
        print(result)
        self.assertIsNotNone(result)

        # Vérifier si la référence a été insérée dans la table "cec_refname"
        self.cursor.execute("SELECT * FROM cec_refname WHERE reference_acc='Reference Acc'")
        result = self.cursor.fetchall()
        print(result)
        self.assertIsNotNone(result)

        # Vérifier si la référence a été insérée dans la table "cec_treeref"
        self.cursor.execute("SELECT * FROM cec_treeref WHERE id_treeref=2")
        result = self.cursor.fetchall()
        print(result)
        self.assertIsNotNone(result)

        # Vérifier si les données ont été insérées dans la table "cec_content"
        self.cursor.execute("SELECT * FROM cec_content WHERE id_cec=2 AND language='French' AND title_name='Title' AND section_name='Section' AND id_treeref=1 AND content='Content'")
        result = self.cursor.fetchall()
        print(result)
        self.assertIsNotNone(result)

        # Vérifier si les données ont été insérées dans la table "cec_reference"
        self.cursor.execute("SELECT * FROM cec_reference WHERE id_cec=1 AND reference_full='Reference Full' AND reference_acc='Reference Acc'")
        result = self.cursor.fetchall()
        print(result)
        self.assertIsNotNone(result)

        # Vérifier si l'entrée a été supprimée de la table "cec_add_datas"
        self.cursor.execute("SELECT * FROM cec_add_datas WHERE id_cec=1")
        result = self.cursor.fetchall()
        print(result)
        self.assertIsNone(result)




# Exécution des tests
if __name__ == '__main__':
    # Lancer les tests avec la couverture de code
    # cov.start()
    unittest.main()
    # cov.stop()
    # cov.save()

    # # Générer le rapport de couverture
    # cov.html_report(directory="coverage_report")
    # cov.erase()

# Supprimer le fichier de base de données de test
#os.remove(DB_TEST)
