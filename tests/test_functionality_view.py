import sqlite3
import unittest
import os

from dktotoolkit.sqlite3 import recursive_sql

from glob_var import DB_TEST, SQL_SCRIPT



# Classe de tests fonctionnels
class TestFunctionality_Tables(unittest.TestCase):

    def setUp(self):

        os.makedirs(os.path.dirname(DB_TEST), exist_ok=True)

        # Connexion à la base de données SQLite
        self.connexion = sqlite3.connect(DB_TEST)
        self.cursor = self.connexion.cursor()

        # Exécution du script SQL
        scripts = recursive_sql(SQL_SCRIPT)
        scripts = scripts if isinstance(scripts, list) else [scripts,]

        for elt in scripts:
            self.cursor.executescript(elt)
        # endFor
    #


    def tearDown(self):
        # Fermeture du curseur et de la connexion
        self.cursor.close()
        self.connexion.close()
        os.remove(DB_TEST)

    def test_content_view(self):
        # Insertion de données dans les tables nécessaires

        # Insérer des données dans cec_languages
        self.cursor.execute("INSERT INTO cec_languages (language) VALUES ('French')")
        self.connexion.commit()

        # Insérer des données dans cec_treeref
        self.cursor.execute("INSERT INTO cec_treeref (id_treeref) VALUES (1)")
        self.connexion.commit()

        # Insérer des données dans cec_treename
        self.cursor.execute("INSERT INTO cec_treename (id_treeref, language, chapter_name) VALUES (1, 'French', 'Introduction')")
        self.connexion.commit()

        # Insérer des données dans cec_content
        self.cursor.execute("INSERT INTO cec_content (id_cec, language, title_name, content) VALUES (1, 'French', 'Introduction11', 'Content contenu')")
        self.connexion.commit()

        # Vérifier si la vue "cec_content_view" renvoie des résultats
        self.cursor.execute("SELECT * FROM cec_content_view")
        results = self.cursor.fetchall()

        self.assertIsNotNone(results)
        #self.assertGreater(len(results), 0)
        self.assertEqual(results, [(1, 'French', None, None, None, None, None, None, 'Introduction11', None, 'Content contenu')])


# Exécution des tests
if __name__ == '__main__':
    # Lancer les tests avec la couverture de code
    unittest.main()
