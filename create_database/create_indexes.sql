
-- Création de l'index sur la colonne "language" de la table "cec_languages"
CREATE INDEX IF NOT EXISTS idx_cec_languages_language ON cec_languages (language);

-- Création de l'index sur les colonnes "id_treeref" et "language" de la table "cec_treename"
CREATE INDEX IF NOT EXISTS idx_cec_treename_id_language ON cec_treename (id_treeref, language);

-- Création de l'index sur la colonne "language" de la table "cec_content"
CREATE INDEX IF NOT EXISTS idx_cec_content_language ON cec_content (language);

-- Création de l'index sur la colonne "reference_acc" de la table "cec_reference"
CREATE INDEX IF NOT EXISTS idx_cec_reference_reference_acc ON cec_reference (reference_acc);


-- Création de l'index sur la colonne "language" de la table "cec_refname"
CREATE INDEX IF NOT EXISTS idx_cec_refname_language ON cec_refname (language);

