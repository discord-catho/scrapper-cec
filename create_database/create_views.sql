-- Création de la vue "cec_content_view" pour afficher le contenu avec les catégories
CREATE VIEW IF NOT EXISTS cec_content_view AS
SELECT
  cc.id_cec,
  cc.language,
  tv.part_id,
  tv.part_name,
  tr.section_id,
  tn.section_name,
  tv.chapter_id,
  tv.chapter_name,
  tv.article_id,
  tv.article_name,
  cc.title_name,
  cc.subparagraph_name,
  cc.content
FROM cec_content cc
LEFT JOIN cec_tree_view tv ON cc.id_treeref = tv.id_treeref AND cc.language = tv.language;


-- Création de la vue "cec_tree_view" pour afficher les catégories de l'arbre
CREATE VIEW IF NOT EXISTS cec_tree_view AS
SELECT
  tr.id_treeref,
  tn.language,
  tr.part_id,
  tn.part_name,
  tr.section_id,
  tn.section_name,
  tr.chapter_id,
  tn.chapter_name,
  tr.article_id,
  tn.article_name,
  tr.paragraph_id,
  tn.paragraph_name
FROM cec_treeref tr
LEFT JOIN cec_treename tn ON tr.id_treeref = tn.id_treeref;
