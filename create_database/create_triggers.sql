-- Déclencheur pour ajouter une langue si elle n'existe pas lors de l'insertion
CREATE TRIGGER IF NOT EXISTS cec_insert_language
BEFORE INSERT ON cec_languages
FOR EACH ROW
BEGIN
  SELECT CASE WHEN EXISTS(SELECT 1 FROM cec_languages WHERE language = NEW.language) THEN
    RAISE(IGNORE)
  END;
END;


-- Déclencheur pour ajouter une référence si elle n'existe pas lors de l'insertion
CREATE TRIGGER IF NOT EXISTS cec_insert_treeref
BEFORE INSERT ON cec_treeref
FOR EACH ROW
BEGIN
  SELECT CASE WHEN EXISTS(SELECT 1 FROM cec_treeref WHERE cec_treeref.id_treeref = NEW.id_treeref) THEN
    RAISE(IGNORE)
  END;
END;


-- Trigger
CREATE TRIGGER IF NOT EXISTS cec_insert_treename
BEFORE INSERT ON cec_treename
FOR EACH ROW
BEGIN
  SELECT CASE WHEN EXISTS(SELECT 1 FROM cec_treename WHERE cec_treename.id_treeref = NEW.id_treeref AND cec_treename.language = NEW.language) THEN
    RAISE(IGNORE)
  END;
END;

-- Trigger
CREATE TRIGGER IF NOT EXISTS cec_insert_content
BEFORE INSERT ON cec_content
FOR EACH ROW
BEGIN
  SELECT CASE WHEN EXISTS(SELECT 1 FROM cec_content WHERE cec_content.id_cec = NEW.id_cec AND cec_content.language = NEW.language) THEN
    RAISE(IGNORE)
  END;
END;


-- ===========================================================

-- Déclencheur pour disperser les données de "cec_add_datas" dans les autres tables
CREATE TRIGGER IF NOT EXISTS cec_insert_add_datas
AFTER INSERT ON cec_add_datas
FOR EACH ROW
BEGIN
  -- Insérer la langue dans cec_languages si elle n'existe pas
  INSERT OR IGNORE INTO cec_languages (language) VALUES (NEW.language);

  -- Insérer la référence dans cec_refname si elle n'existe pas
  INSERT OR IGNORE INTO cec_refname (reference_acc, language) VALUES (NEW.reference_acc, NEW.language);

  -- Insérer la référence dans cec_treeref si elle n'existe pas
  INSERT OR IGNORE INTO cec_treeref (id_treeref) VALUES (NEW.id_treeref);

  -- Insérer les données dans cec_content uniquement si elles n'existent pas déjà
  INSERT INTO cec_content (id_cec, language, title_name, subparagraph_name, id_treeref, content)
  SELECT NEW.id_cec, NEW.language, NEW.title_name, NEW.subparagraph_name, NEW.id_treeref, NEW.content
  WHERE NOT EXISTS (
    SELECT 1 FROM cec_content WHERE id_cec = NEW.id_cec AND language = NEW.language
  );

  -- Insérer les données dans cec_reference uniquement si elles n'existent pas déjà
  INSERT INTO cec_reference (id_cec, reference_full, reference_acc)
  SELECT NEW.id_cec, NEW.reference_full, NEW.reference_acc
  WHERE NOT EXISTS (
    SELECT 1 FROM cec_reference WHERE id_cec = NEW.id_cec AND reference_acc = NEW.reference_acc
  );

  -- Supprimer l'entrée de la table cec_add_datas
  DELETE FROM cec_add_datas WHERE id_cec = NEW.id_cec;
END;





-- Déclencheur pour insérer les données dans les tables "cec_treeref" et "cec_treename"
CREATE TRIGGER IF NOT EXISTS cec_insert_cec_add_tree
AFTER INSERT ON cec_add_tree
FOR EACH ROW
BEGIN
  -- Insérer les données dans cec_treeref uniquement si elles n'existent pas déjà
  INSERT INTO cec_treeref (id_treeref, part_id, section_id, chapter_id, article_id, paragraph_id)
  SELECT NEW.id_treeref, NEW.part_id, NEW.section_id, NEW.chapter_id, NEW.article_id, NEW.paragraph_id
  WHERE NOT EXISTS (
    SELECT 1 FROM cec_treeref WHERE id_treeref = NEW.id_treeref
  );


  -- Insérer les données dans cec_treename uniquement si elles n'existent pas déjà
  INSERT INTO cec_treename (id_treeref, language, part_name, section_name, chapter_name, article_name, paragraph_name, url)
  SELECT NEW.id_treeref, NEW.language, NEW.part_name, NEW.section_name, NEW.chapter_name, NEW.article_name, NEW.paragraph_name, NEW.url
  WHERE NOT EXISTS (
    SELECT 1 FROM cec_treename WHERE id_treeref = NEW.id_treeref AND language = NEW.language
  );

   -- Supprimer l'entrée de la table cec_add_datas
   DELETE FROM cec_add_tree WHERE id_treeref = NEW.id_treeref;
END;
