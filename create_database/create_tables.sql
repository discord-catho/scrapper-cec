-- Création de la table "cec_languages" pour stocker les langues
CREATE TABLE IF NOT EXISTS cec_languages (
  id_language INTEGER PRIMARY KEY AUTOINCREMENT,
  language TEXT NOT NULL UNIQUE
);




-- Création de la table "cec_treeref" pour stocker les références de l'arbre de catégories
CREATE TABLE IF NOT EXISTS cec_treeref (
  id_treeref INTEGER PRIMARY KEY,
  part_id INTEGER,
  section_id INTEGER,
  chapter_id INTEGER,
  article_id INTEGER,
  paragraph_id INTEGER
);




-- Création de la table "cec_treename" pour stocker les noms des catégories de l'arbre
CREATE TABLE IF NOT EXISTS cec_treename (
  id_treeref INTEGER NOT NULL,
  language TEXT NOT NULL,
  part_name TEXT,
  section_name TEXT,
  chapter_name TEXT,
  article_name TEXT,
  paragraph_name TEXT,
  url TEXT,
  PRIMARY KEY (id_treeref, language),
  FOREIGN KEY (id_treeref) REFERENCES cec_treeref (id_treeref),
  FOREIGN KEY (language) REFERENCES cec_languages (language)
);


-- Création de la table "cec_content" pour stocker le contenu
CREATE TABLE IF NOT EXISTS cec_content (
  id_cec INTEGER NOT NULL,
  language TEXT NOT NULL,
  title_name TEXT,
  subparagraph_name TEXT,
  id_treeref INTEGER,
  content TEXT,
  PRIMARY KEY (id_cec, language),
  FOREIGN KEY (id_treeref) REFERENCES cec_treeref (id_treeref),
  FOREIGN KEY (language) REFERENCES cec_languages (language)
);


-- Création de la table "cec_reference" pour stocker les références
CREATE TABLE IF NOT EXISTS cec_reference (
  id_ref INTEGER PRIMARY KEY AUTOINCREMENT,
  id_cec INTEGER NOT NULL,
  reference_full TEXT,
  reference_acc TEXT NOT NULL,
  FOREIGN KEY (reference_acc) REFERENCES cec_refname (reference_acc),
  FOREIGN KEY (id_cec) REFERENCES cec_content (id_cec)
);

-- Création de la table "cec_refname" pour stocker les noms des références
CREATE TABLE IF NOT EXISTS cec_refname (
  reference_acc TEXT NOT NULL UNIQUE,
  reference_name TEXT,
  language TEXT,
  PRIMARY KEY (reference_acc, language),
  FOREIGN KEY (language) REFERENCES cec_languages (language)
);



-- =====================================


-- Création de la table "cec_add_tree"
CREATE TABLE IF NOT EXISTS cec_add_tree (
  id_treeref INTEGER PRIMARY KEY,
  part_id INTEGER,
  section_id INTEGER,
  chapter_id INTEGER,
  article_id INTEGER,
  paragraph_id INTEGER,
  language TEXT NOT NULL,
  part_name TEXT,
  section_name TEXT,
  chapter_name TEXT,
  article_name TEXT,
  paragraph_name TEXT,
  url TEXT
);


-- Création de la table "cec_add_datas" avec toutes les colonnes des autres tables
CREATE TABLE IF NOT EXISTS cec_add_datas (
  id_cec INTEGER PRIMARY KEY,
  language TEXT,
  title_name TEXT,
  subparagraph_name TEXT,
  id_treeref INTEGER,
  content TEXT,
  reference_full TEXT,
  reference_acc TEXT
);
