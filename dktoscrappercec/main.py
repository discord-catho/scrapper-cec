import os

from lib.getdatas_idx import GetIndex
from lib.getdatas_content import GetDatas


def main():
    url_index = "https://www.vatican.va/archive/FRA0013/_INDEX.HTM"


    idx = GetIndex(url_index)
    idx.write_db("./datas/coucou.db")
    base_url = "https://www.vatican.va/archive/FRA0013"

    c = idx.to_list()
    datas = []
    for elt in c[10:12]:

        url_page = os.path.join(base_url, elt)
        print(url_page)
        d = GetDatas(url_page)
        d.write_db("./datas/coucou.db")
    #

    pass

if __name__=="__main__":
    main()
