from ._structureddatas2list import structureddatas2list
from ._clean_list4db import clean_list4db


def content_structured_to_list(datas):
    list_datas = _structureddatas2list(datas)
    return _clean_list4db(list_datas)
