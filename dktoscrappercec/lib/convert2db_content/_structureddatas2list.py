from ._var2sublist import var2sublist

def structureddatas2list(datas):
    out_datas = []

    for each_data in datas:
        informations = each_data["informations"]

        url = informations["original_url"]
        partie, section, chapitre, article = informations["tree"] + [None] * (4 - len(informations["tree"]))

        #id_tree = ENVOYER (partie, section, chapitre, article) : message warningsi n existe pas ET RECUPERER ID
        id_tree = None
        page = each_data["page"]

        for elt_page in page:
            ref, content, title, subtitle = [None]*4
            title = elt_page["title"]
            subtitle = None

            if title == "None":
                continue
            #
            if not "datas" in elt_page.keys():
                raise ValueError(elt_page)
            #


            for elt in elt_page["datas"]:
                ref, content, subtitle = [None]*3
                if isinstance(elt, str):
                    subtitle = elt
                    out_datas.append(
                        var2sublist(
                            id_tree=id_tree,
                            title=title,
                            subtitle=subtitle,
                            id_cec=ref,
                            content=content
                        )
                    )
                elif "subtitle" in elt.keys():
                    subtitle = elt["subtitle"]

                    if not "datas" in elt.keys():
                        elt["datas"] = []
                    for subelt in elt["datas"]:
                        ref, content = [None]*2
                        print("++>", isinstance(subelt, dict), subelt.keys())
                        if isinstance(subelt, dict) and "content" in subelt.keys():
                            ref = subelt["ref"]
                            content = subelt["content"]

                            out_datas.append(
                                var2sublist(
                                    id_tree=id_tree,
                                    title=title,
                                    subtitle=subtitle,
                                    id_cec=ref,
                                    content=content
                                )
                            )
                        else:
                            print(elt)
                            raise ValueError(f"SUBELT  {subelt}")
                        #
                    print(ref)
                elif "content" in elt.keys():
                    ref = elt["ref"]
                    content = elt["content"]
                    out_datas.append(
                        var2sublist(
                            id_tree=id_tree,
                            title=title,
                            subtitle=subtitle,
                            id_cec=ref,
                            content=content
                        )
                    )
                else:
                    print("ERROR : each_datas", each_datas)
                    print("ERROR : elt", elt)
                    raise ValueError("Voir les deux messages au dessus !")
                #
            #
        # endFor elt in page
    # endFor each_datas

    return out_datas
#
