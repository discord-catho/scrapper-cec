def var2sublist(
        id_tree=None,
        title=None,
        subtitle=None,
        id_cec=None,
        content=None,
        language=None
):
    """Create a sublist from items (usefull, to keep the order)
:param int id_tree: Build from the index
:param str title: Title of the page (the last tree element, en principe)
:param str subtitle: A subtitle, if present
:param int id_cec: Paragraph number in the CEC
:param str content: The paragraph
"""
    return {
        "id_treeref":id_tree,
        "title_name":title,
        "paragraph_name":subtitle,
        "id_cec":id_cec,
        "content":content,
        "language":language,
        "reference_full":None,
        "reference_acc":None
    }
#
