import html
import requests
def html_request(url = "https://www.vatican.va/archive/FRA0013/_INDEX.HTM"):
    r = requests.get(url)
    html_content = html.unescape(r.text)
    return html_content
# endDef


def read_file(path = "./index.html"):
    with open(path, 'r') as f:
        content = f.read()
    # endWith
    #return content
    return html.unescape(content)



