from ._text2num import titre2num, article2num, paragraph2num


def flatten_toc(toc,
                id_treeref=1, language=None,
                part_id=None,    part_name=None,
                section_id=None, section_name=None,
                chapter_id=None, chapter_name=None,
                article_id=None, article_name=None):
    flattened_toc = []

    if isinstance(toc, dict):
        toc = toc["content"]
    #

    for item in toc:
        paragraph_id = None
        paragraph_name = None
        url = None
        if 'url' in item:
            url = item['url']
        #
        if 'text' in item:
            text = item["text"]
            if "partie" in text.lower():
                part_id, part_name = titre2num(text)
            elif "section" in text.lower():
                section_id, section_name = titre2num(text)
            elif "chapitre" in text.lower():
                chapter_id, chapter_name = titre2num(text)
            elif "article" in text.lower():
                article_id, article_name = article2num(text)
            else:
                paragraph_id, paragraph_name = paragraph2num(text)
            #
            flattened_toc.append({
                'id_treeref': id_treeref,
                'part_id': part_id,
                'part_name': part_name,
                'section_id': section_id,
                'section_name': section_name,
                'chapter_id': chapter_id,
                'chapter_name': chapter_name,
                'article_id': article_id,
                'article_name': article_name,
                'paragraph_id':paragraph_id,
                'paragraph_name':paragraph_name,
                'url': None,
                'language':language
            })
            id_treeref += 1
        elif 'content' in item:
            if 'text' in item:  # ca doit corriger un bug, jsp lequel...
                section_name = item['text']
                section_id = unique_id
                id_treeref += 1
            else:
                section_name = section_name
                section_id = section_id
            #
            sub_toc = flatten_toc(
                item['content'],
                id_treeref,
                language,
                part_id,
                part_name,
                section_id,
                section_name,
                chapter_id,
                chapter_name,
                article_id,
                article_name,
            )
            flattened_toc.extend(sub_toc)
            id_treeref += len(sub_toc)
        else:
            print("coucou")
    return flattened_toc
