#from ._idx2db import idx2db
from ._text2num import titre2num, article2num, paragraph2num
from ._flatten_toc import flatten_toc


__all__ = [
    "titre2num", "article2num", "paragraph2num","flatten_toc"
    ]
