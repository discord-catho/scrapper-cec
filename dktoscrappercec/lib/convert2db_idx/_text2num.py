def titre2num(titre):
    num = None
    if "premier" in titre.lower():
        num = 1
    elif "second" in titre.lower() or "deuxième" in titre.lower():
        num = 2
    elif "troisième" in titre.lower():
        num = 3
    elif "quatrième":
        num = 4
    elif "cinquième":
        num = 5
    else:
        num = 99
    #
    return num, " ".join(titre.split()[2:]).strip()

def article2num(titre):
    num = int(titre.split()[1])
    return num, " ".join(titre.split()[2:]).strip()

def paragraph2num(titre):
    roman2latin = {"I":1, "II":2, "III":3, "IV":4, "V":5, "VI":6, "VII":7, "VIII":8, "IX": 9, "X":10}

    if "paragraphe" in titre.lower():
        num = int(titre.split()[1].replace(".",""))
        return num, " ".join(titre.split()[2:]).strip()
    elif titre.split(".")[0] in roman2latin.keys():
        return roman2latin[titre.split(".")[0]], " ".join(titre.split(".")[1:]).strip()
    elif titre.split()[0] in roman2latin.keys():
        return roman2latin[titre.split()[0]], " ".join(titre.split()[1:]).strip()
    else:
        return 98, titre
