import os
import json

def write_datas(datas, fname):
    with open(fname, 'w') as f:
        json.dump(datas, f, ensure_ascii=False, indent=2)
    # endWith
#


def datas2json(datas, url):
    directory = "datas"
    os.makedirs(directory, exist_ok=True)

    json_name = os.path.splitext(os.path.basename(url))[0]+".json"
    write_datas(datas, os.path.join(directory, json_name))
#
