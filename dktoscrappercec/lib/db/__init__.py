import os
from threading import Lock # verrou

from dktotoolkit.envvar import assign_envvar
from dktoeasysqlite3 import MyDB


# TODO : ajouter possibilité de ne pas avoir 1 db ici
#        mais seulement un ensemble de tables

class CEC_DB(MyDB):
    def __init__(self,
                 db_path:str=None,
                 db_path_env:str="DB_CEC_PATH",
                 db_prefix:str=None,  # Pas implemente
                 db_prefix_env:str="cec",
                 dbtable_language:str=None,
                 dbtable_language_env="DB_CEC_LANGUAGE",
                 dbtable_treeref:str=None,
                 dbtable_treeref_env:str="DB_CEC_TREEREF",
                 dbtable_treename:str=None,
                 dbtable_treename_env:str="DB_CEC_TREENAME",
                 dbtable_content:str=None,
                 dbtable_content_env:str="DB_CEC_CONTENT",
                 dbtable_reference:str=None,
                 dbtable_reference_env:str="DB_CEC_REFERENCE",
                 dbtable_refname:str=None,
                 dbtable_refname_env:str="DB_CEC_REFNAME"
                 ):

        useenv=False

        self.db_path = assign_envvar(
            db_path,
            db_path_env,
            "./datas/cec.db",
            useenv=useenv
        )

        self.dbtable_language = assign_envvar(
            dbtable_language,
            dbtable_language_env,
            "cec_languages",
            useenv=useenv
        )

        self.dbtable_treeref = assign_envvar(
            dbtable_treeref,
            dbtable_treeref_env,
            "cec_treeref",
            useenv=useenv
        )
        self.dbtable_treename = assign_envvar(
            dbtable_treename,
            dbtable_treename_env,
            "cec_treename",
            useenv=useenv
        )

        self.dbtable_content = assign_envvar(
            dbtable_content,
            dbtable_content_env,
            "cec_content",
            useenv=useenv
        )

        self.dbtable_reference = assign_envvar(
            dbtable_reference,
            dbtable_reference_env,
            "cec_reference",
            useenv=useenv
        )
        self.dbtable_refname = assign_envvar(
            dbtable_refname,
            dbtable_refname_env,
            "cec_refname",
            useenv=useenv
        )


        super().__init__(
            lock_in=Lock(),
            createIfNotExists="../create_database/create_database.sql"
        )

    #endDef

#endClass
