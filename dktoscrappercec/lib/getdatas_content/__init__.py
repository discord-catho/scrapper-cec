import os

from dktotoolkit.list import aplatir_liste
from ._parse_page import parse_page
from ..db import CEC_DB
from ..convert2db_content.content_structured_to_list import content_structured_to_list


class GetDatas():

    def __init__(self, url_index, db_name="./datas/cec.db"):
        """Creer la structure de l'index
        :param str url_index: url de l'index
        :param str db_name: Path to the database
        :returns: structure of the index
        :rtypes: tuple
        """
        self.url_index = url_index
        self.db_name = db_name
        self.structured = parse_page(url_index, db_name=self.db_name)

    #

    def get_datas(self):
        return self.structured

    def write_json(self):
        return datas2json(self.structured, url_index)

    def write_db(self):
        """
        :param list datas: List of structure, json like array
        """
        # ToDo : check if datas exists.

        if isinstance(self.structured, dict):
            self.structured = [self.structured,]
        elif not isinstance(self.structured, list):
            raise ValueError(type(self.structured))
        #

        # Reconstruire les numerotations des chapitres via index_datas et ENVOYER l'ARBRE (creation ID)


        cec_db = CEC_DB(db_path=self.db_name)
        for elt_page in self.structured:
            print(">>", elt_page)
            if isinstance(elt_page, dict) or not "datas" in elt_page.keys():
                continue
            #
            for elt_datas in elt_page["datas"]:
                cec_db.insert_data(
                    table_name="cec_add_datas",
                    data_dict=elt_datas,
                    allow_duplicates=False,
                    commit=True)
        #
        cec_db.commit()
        cec_db.close()
    #

    #return idx2db(self.structured, db_name)

