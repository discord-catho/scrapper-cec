import requests
import html
import json
from bs4 import BeautifulSoup, NavigableString, element
from dktotoolkit.html import request_html_page

from .._clean import cleanjson_loop


def clean_htmlline(s):
    """Nettoyage de texte
:param str s: Texte d'entree
:return: s : Texte de sortie, nettoyé
:rtypes: string
"""
    s = s.replace("\xa0"," ")
    #s = s.replace("\n", " ")
    s = s.replace("\r", " ")
    s = s.replace('\"', '"')

    return s


def parse_bsline(e):
    """
    Parser la ligne bs

    :param BeautifulSoup elt: The bs4 data (one ligne)
    :return: {ref (is digit present at beginning), content}
    :rtypes: dict(str:str)
"""

    dd = [clean_htmlline(a)  for a in list(e.descendants) if a == a.string and a.split()]

    if not isinstance(dd, list) or not dd:
        pass
    elif len(dd) == 1:
        return {"content":str(dd[0])}
    elif dd[0].isdigit():
        return {"ref":int(dd[0]), "content":str(" ".join(dd[1:]))}
    else:
        return {"content":str(" ".join(dd[:]))}
    # endIf

# endDef


def _group_datas(datas, i):
    """
    :param list dict: Datas
    :param int i: position where to have a look, if to add to i-1
"""

    if (not "ref" in datas[i].keys()) :

        target_str = datas[i-1]["content"]

        if (
                ("style" in datas[i].keys())  and
                (datas[i]["style"] == "margin-left") ):

            to_add_str = f"<quote>{datas[i]['content']}</quote>"
        else:
            to_add_str = f"{datas[i]['content']}"
        # endIf not style inside

        datas[i-1]["content"] = f"""{target_str}<br/>\n{to_add_str}"""
        del  datas[i]

    else:

        pass
    # endIf not ref inside

    return datas
# endDef


def _post_struct(datas):
    """Remettre des paragraphes dans leurs references"""

    if isinstance(datas, int):
        return datas

    elif isinstance(datas, dict):

        return {_post_struct(k): _post_struct(v) for k,v in datas.items()}

    elif isinstance(datas, str):

        return datas

    elif isinstance(datas, list) or isinstance(datas, tuple):

        new_datas = datas

        n = len(datas)

        for i in range(n-1, -1,-1):

            if i > 0:
                d0 = isinstance(datas[i], dict)
                d1 = isinstance(datas[i-1], dict)
                if d0 and d1 :
                    c0 = "content" in datas[i].keys()
                    c1 = "content" in datas[i-1].keys()
                    r0 = not"ref" in datas[i].keys()
                    r1 = "ref" in datas[i-1].keys()
                    mergetop = d0 and d1 and c0 and c1 and r0 and r1
                else:
                    continue
                    mergetop = False
                #
                if mergetop :

                    quote = (
                        "style" in datas[i].keys()
                        and
                        datas[i]["style"] == "margin-left"
                    )

                    target_str = datas[i-1]["content"]

                    if ( quote ):
                        to_add_str = f"<quote>{datas[i]['content']}</quote>"
                    else:
                        to_add_str = f"{datas[i]['content']}"
                    # endIf not style inside

                    datas[i-1]["content"] = f"""{target_str}<br/>\n{to_add_str}"""

                    del  datas[i]

                # endIf

            # endIf
        # endFor

        return [_post_struct(e) for e in datas]

    else:

        return datas

    # endIf datas

    return datas
# endDef


def fetch_title(soup):
    # Extraire le titre
    title_element = soup.find('title')
    return (title_element.text)
# endDef


def fetch_tree(soup):
    # Extraire le contenu de l'arborescence
    tree_elements = soup.select('li')
    return [str(e) for e in soup.li.descendants if e == e.string]
# endDef


def parse_page(url):
    """Parser des pages de contenu du CEC ; pour l'index, cf :func:`parser_index`

    :param str url: URL de la page a parser
    :return: The datas, structured, of the page, in a json-like dict
    :rtype: dict(str)
    """

    DEFAULT_TITLE = "untitled"
    html_content = request_html_page(url)

    soup = BeautifulSoup(html_content, 'html.parser')


    title = fetch_title(soup)
    tree =  fetch_tree(soup)

    list_paragraphs = soup.body.find_all("p", recursive=False)

    title = tree[-1]
    d = {
                "title":title,
                "datas":[]
            }

    datas = {
        "informations":{
            "origin": title,
            "tree" : tree,
        },
        "page":[d]
    }


    subsubtitle = ""
    i = 0

    for elt in list_paragraphs:

        elt_cont = elt.contents

        if elt is not None and elt.attrs and "style" in elt.attrs.keys():
            style = str(elt["style"].split(":")[0])
        else:
            style = None
        #

        if len(elt_cont) == 1 and elt.b:

            # Les titres et sous-titres sont en gras...

            if "style" in elt.b.attrs:

                # ... mais seul le titre a un attribue "style"
                title = str(elt_cont[0].string)
                datas["page"].append({})
                datas["page"][-1]["title"] = title
                datas["page"][-1]["datas"] = []

            else:

                subsubtitle = str(elt_cont[0].string)
                datas["page"][-1]["datas"].append(
                        {"subtitle":subsubtitle,
                         "datas":[]
                         }
                )

            # endIf

        elif (
                isinstance(datas["page"][-1]["datas"], dict) and
                subsubtitle in datas["page"][-1]["datas"].keys()
        ):

            # Cas le plus simple : on a un titre *et* un titre de paragraphe
            # ... inutile ?

            elt_add = parse_bsline(elt)
            if not elt_add:
                continue
            elif style:
                elt_add["style"] = style
            #

            datas["page"][-1]["datas"][-1].append(
                {
                    "subtitle":subsubtitle,
                    "datas":elt_add
                }
            )

            print("INATTENDU", datas["page"][-1]["datas"][subtitle][-1])
            raise ValueError(str(elt_add))

        elif (
                isinstance(datas["page"][-1]["datas"], list) and
                len(datas["page"][-1]["datas"]) > 1 and
                isinstance(datas["page"][-1]["datas"][-1], dict) and
                "subtitle" in datas["page"][-1]["datas"][-1].keys()
        ):
            # On a un titre et plusieurs titres de paragraphe

            elt_add = parse_bsline(elt)
            if not elt_add:
                continue
            elif style:
                elt_add["style"] = style
            #

            datas["page"][-1]["datas"][-1]["datas"].append(elt_add)

        elif (
                isinstance(datas["page"][-1]["datas"], list) #and
                #len(datas["page"][-1]["title"]) == 0
                #and len(datas["page"][-1]["datas"]) > 1
                #and not "subtitle" in datas["page"][-1]["datas"][-1].keys()
        ):

            # On a un titre, mais pas de titre de paragraphe

            elt_add = parse_bsline(elt)
            if not elt_add:
                continue
            elif style:
                elt_add["style"] = style
            #

            datas["page"][-1]["datas"].append(elt_add)

        else:

            print(">> IF10")
            print(isinstance(datas["page"][-1]["datas"], list) )
            print(len(datas["page"][-1]["datas"]) > 1 )
            #print(isinstance(datas["page"][-1]["datas"][-1], dict))
            #print("subtitle" in datas["page"][-1]["datas"][-1].keys())
            print(elt)
            print("ERR ERR ERR ERR ERR ERR ERR ERR ERR ERR ERR")
            raise ValueError("Excpetion")
        #
        i += 1
    #
    datas["informations"]["original_url"] = url

    datas = cleanjson_loop(datas)

    datas = _post_struct(datas)

    return datas


if __name__=="__main__":
    a = parse_page()
    #print(a)
