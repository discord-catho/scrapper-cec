def cleanjson_loop(struct, nmax=10):
    """
    :param dict|list struct: json-like dict or list
    :param int nmax: maximal number of iterations (default: 10)
    :return: a json-like cleaned structure
    :rtypes: dict|list
    """

    struct2 = []

    i = 0

    while struct != struct2 and i<nmax:
        print("LOOP", i)
        struct2 = removeUselessList(struct)
        struct = cleandatas(struct2)
        i+=1
    # endWhile

    return struct

# endDef


def removeUselessList(elt):
    if isinstance(elt, str):
        return elt
    elif isinstance(elt, dict):
        return {removeUselessList(k):removeUselessList(v) for k,v in elt.items() if k and v}
    elif (not isinstance(elt, list) and not isinstance(elt, tuple)):
        return elt
    elif len(elt) == 0:
        return None
    elif len(elt) == 1 and elt[0]:
        return removeUselessList(elt[0])
    else:
        return [removeUselessList(e) for e in elt if e]
    #
#


def cleandatas(elt):

    if isinstance(elt, str):
        return elt
    elif (isinstance(elt, list) or isinstance(elt, tuple)) and len(elt) == 1:
        return cleandatas(elt[0])
    elif (isinstance(elt, list) or isinstance(elt, tuple)) :
        return [cleandatas(e) for e in elt if e]
    elif isinstance(elt, dict) and "text" in elt.keys() and (not elt["text"] or elt["text"] == "\n"):
        return None
    elif (
            isinstance(elt, dict) and
            "content" in elt.keys() and
            isinstance(elt["content"], dict)
    ):

        subc = elt["content"]
        del elt["content"]

        return {
            **{cleandatas(k):cleandatas(v) for k, v in elt.items()},
            **{cleandatas(k):cleandatas(v) for k, v in subc.items()}
            }
    elif isinstance(elt, dict):
        return {cleandatas(k):cleandatas(v) for k, v in elt.items()}
    else:
        return elt
    #
#


def converge_list(func, input_list, itemax=150):

    a = []
    b = func(input_list) # doit retourner une liste
    i = 0
    while a != b and i < itemax:
        print("ite : ", i)
        a = func(b)
        b = func(a)
        print("......", len(b), len(a))
        i += 1
    #
    return b
