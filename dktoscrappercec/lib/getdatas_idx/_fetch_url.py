def fetch_url(datas):
    if isinstance(datas, list) or isinstance(datas, tuple):

        return [fetch_url(e) for e in datas if e]

    elif isinstance(datas, dict) and "url" in datas.keys():

        u = datas["url"]
        del datas["url"]
        return [u, *[fetch_url(e) for e in datas.values() if e]]

    elif isinstance(datas, dict):

        return [fetch_url(v) for v in datas.values() if v]

    elif isinstance(datas, str):
        pass
    else:
        pass
    #

#
