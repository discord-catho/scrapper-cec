import os

from dktotoolkit.list import aplatir_liste

from ._parse_index import parse_index
from ._fetch_url import fetch_url
#from ..convert2db_idx import idx2db
from ..db import CEC_DB
from ..convert2db_idx._flatten_toc import flatten_toc

class GetIndex():

    def __init__(self, url_index, db_name):
        """Creer la structure de l'index
        :param str url_index: url de l'index
        :param str db_name: Path to the database
        :returns: structure of the index
        :rtypes: tuple
        """
        self.url_index = url_index
        self.db_name = db_name
        self.structured = parse_index(url_index)

    #

    def get_datas(self):
        return self.structured

    def to_list(self):
        urls = fetch_url(self.structured)
        return aplatir_liste(urls, doubles=False)

    def write_json(self):
        return datas2json(self.structured, url_index)

    def write_db(self,
                 language="French",
                 base_url = "https://www.vatican.va/archive/FRA0013"
                 ):
        """
        :param list datas: List of structure (index), json like array
        """

        f = flatten_toc(self.structured, language=language)

        #for e in f[:30]:
        #    print("\t\t", e, "\n")
        #

        cec_db = CEC_DB(db_path=self.db_name)

        for elt in f:
            print(">>", elt) # TODO : ajouter l'url de l'element (dans parse_index ?)

            cec_db.insert_data(
                table_name="cec_add_tree",
                data_dict=elt,
                commit=True)
        #
        cec_db.commit()
        cec_db.close()

        #return idx2db(self.structured, db_name)

