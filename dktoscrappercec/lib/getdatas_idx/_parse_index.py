from bs4 import BeautifulSoup, NavigableString, element
from dktotoolkit.html import request_html_page

from .._clean import cleanjson_loop


def extract_urls(datas):
    if isinstance(datas, dict) and "url" in datas.keys():
        return datas["url"]
    elif isinstance(datas, dict):
        return [extract_urls(v) for v in datas.values()]
    elif isinstance(datas, list) or isinstance(datas, tuple):
        return [extract_urls(e) for e in datas if e]
    #
    return extract_urls(datas)
#


def _rec_parse_idx(elt, nmax=150):
    """
    Convert page to a json-like dict, NOT CLEAN

    :param BeautifulSoup elt: HTML content (! not an str !)
    :param int nmax: Number max of iteration (default: 150),
but not the max number of recursions !
    :return: dict/list structure (as json) with only text and urls
    """

    # Cette fonction est ok, le resultat est a nettoyer

    i = 0
    while elt != None and i < nmax:

        if (
                isinstance(elt, NavigableString) or
                isinstance(elt, element.NavigableString) or
                isinstance(elt, str)
        ):

            return {"text":elt}

        elif isinstance(elt, list) :

            return elt

        elif elt.a and "href" in elt.a.attrs:

            return {
                "url":elt.a["href"],
                "content":[_rec_parse_idx(e) for e in elt.children]
            }

        else:

            return [_rec_parse_idx(e) for e in elt.children]

        # endIf

        i += 1

    # endWhile

    return None
# endDef


def parse_index(url):
    """Recuperer et parser le contenu de la page d'index
:param str url: url de l'index du CEC
:return struct1: JSon-like structure (dict, list)
:rtypes: dict
"""
    html_content = request_html_page(url)

    soup = BeautifulSoup(html_content, 'html.parser')
    body = soup.body.ul
    struct1 = _rec_parse_idx(body)
    struct1 = cleanjson_loop(struct1)

    return struct1
